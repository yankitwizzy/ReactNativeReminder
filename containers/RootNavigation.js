import React from 'react';
import {connect} from 'react-redux';
import {AsyncStorage} from 'react-native';
import * as actionCreators from '../redux/reducers/reminders';
import MainTabNavigator from '../navigation/MainTabNavigator';
import ReminderDetailsScreen from '../screens/ReminderDetailsScreen';
//import registerForPushNotificationsAsync from '../api/registerForPushNotificationsAsync';
import {StackNavigator} from 'react-navigation';
import {LocaleProvider} from 'antd-mobile';
import enUs from 'antd-mobile/lib/date-picker/locale/en_US';
import {Notifications} from 'expo';

const RootStackNavigator = StackNavigator(
    {
        Main: {
            screen: MainTabNavigator,
        },
        ReminderDetails: {
            screen: ReminderDetailsScreen,
        },
    },
    {
        navigationOptions: () => ({
            headerTitleStyle: {
                fontWeight: 'normal',
            },
        }),
    }
);

const RootNavigator = class RootNavigator extends React.Component {
    componentDidMount () {
        this._notificationSubscription = this._registerForPushNotifications();
    }

    componentWillMount () {
        const context = this;
        this.fetchDataFromDb(context);
    }

    componentWillUnmount () {
        this._notificationSubscription && this._notificationSubscription.remove();
    }

    render () {
        return (<LocaleProvider locale={enUs}><RootStackNavigator
            screenProps={this.props}/></LocaleProvider>);
    }

    _registerForPushNotifications () {
        // Send our push token over to our backend so we can receive notifications
        // You can comment the following line out if you want to stop receiving
        // a notification every time you open the app. Check out the source
        // for this function in api/registerForPushNotificationsAsync.js
        //registerForPushNotificationsAsync();

        // Watch for incoming notifications
        this._notificationSubscription = Notifications.addListener(
            this._handleNotification
        );
    }

    async fetchDataFromDb (context) {
        try {
            const value = await AsyncStorage.getItem("data");
            if (value !== null) {
                // We have data!!
                let state = JSON.parse(value);
                context.props.fetchedReminders(state);
            }
        } catch (error) {
            // Error retrieving data
            console.log("error retrieving data from db in main " + error);
        }
    }

    _handleNotification = ({origin, data}) => {
        console.log(
            `Push notification ${origin} with data: ${JSON.stringify(data)}`
        );
        if (data.last) {
            this.props.rescheduleReminder(data);
            setTimeout(() => {
                this.props.rescheduleNotificationsForReminderAction(data);
            }, 500);
            // this.props.rescheduleReminder(data);
        }
    };
};

const createActionDispatchers = actionCreators => dispatch =>
    Object.keys(actionCreators).reduce((actionDispatchers, name) => {
        var actionCreator = actionCreators[name]
        if (typeof actionCreator == 'function') {
            actionDispatchers[name] = (...args) => dispatch(actionCreator(...args))
        }
        return actionDispatchers
    }, {});

const mapStateToProps = state => ({
    reminders: state.reminders,
    filteredReminders: state.filteredReminders,
    searching: state.searching,
    settings: state.settings
});
const mapDispatchToProps = createActionDispatchers(actionCreators);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RootNavigator);