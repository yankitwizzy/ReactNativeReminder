import React from 'react';
import {ScrollView, StyleSheet, View, Linking} from 'react-native';
import {List, Picker, DatePicker} from 'antd-mobile';
import FormConstants from '../constants/FormConstants';
import moment from 'moment';
import {Constants} from "expo";
import {createForm} from 'rc-form';
import enUs from 'antd-mobile/lib/date-picker/locale/en_US';

class SettingsScreen extends React.Component {
    static navigationOptions = {
        title: 'Settings',
        headerRight: null,
        headerLeft: null
    };

    constructor (props) {
        super(props);
        this.handleDateChange = this.handleDateChange.bind(this);
    }

    handleDateChange (val, field) {
        this.props.screenProps.updateSetting(val, field);
    }

    render () {

        const {getFieldProps} = this.props.form;
        const handleChange = this.handleDateChange;

        return (
            <ScrollView style={styles.container}>
                <List renderHeader={() => 'Default Notification Settings'}>
                    <DatePicker locale={enUs}
                                extra=""
                                onChange={this.handleDateChange}
                                mode="time"
                                {...getFieldProps('startTime',
                                    {
                                        onChange (val) {
                                            handleChange(val, "startTime");
                                        },
                                        initialValue: moment(this.props.screenProps.settings.startTime)
                                    })}
                                minuteStep={1}>
                        <List.Item thumb="http://bugme.talonsoftwares.com/images/start-icon2.png" arrow="horizontal">Start Time</List.Item>
                    </DatePicker>
                    <DatePicker locale={enUs} extra=""
                                onChange={this.handleDateChange}
                                mode="time"
                                {...getFieldProps('endTime',
                                    {
                                        onChange (val) {
                                            handleChange(val, "endTime");
                                        },
                                        initialValue: moment(this.props.screenProps.settings.endTime)
                                    })}
                                minuteStep={1}>
                        <List.Item thumb="http://bugme.talonsoftwares.com/images/stop.png" arrow="horizontal">End Time</List.Item>
                    </DatePicker>
                    <Picker {...getFieldProps('frequency', {initialValue: [this.props.screenProps.settings.frequency]})}
                            title="Frequency"
                            okText="Ok"
                            onChange={(val) => {
                                this.props.screenProps.updateSetting(val, "frequency");
                            }}
                            dismissText="Cancel"
                            data={FormConstants.reminderFrequency}>
                        <List.Item thumb="http://bugme.talonsoftwares.com/images/sound.png" arrow="horizontal"
                                   onClick={this.onClick}>Frequency</List.Item>
                    </Picker>
                    <Picker {...getFieldProps('interval', {initialValue: [this.props.screenProps.settings.interval]})}
                            title="Interval"
                            okText="Ok"
                            onChange={(val) => {
                                this.props.screenProps.updateSetting(val, "interval");
                            }}
                            dismissText="Cancel"
                            data={FormConstants.reminderInterval}>
                        <List.Item thumb="http://bugme.talonsoftwares.com/images/interval.png" arrow="horizontal"
                                   onClick={this.onClick}>Interval</List.Item>
                    </Picker>
                </List>
                <List renderHeader={() => 'About BugMe'}>
                    <List.Item multipleLine
                               thumb="http://bugme.talonsoftwares.com/images/alarm-clock.png">{Constants.manifest.name}
                        <List.Item.Brief>{Constants.manifest.description}</List.Item.Brief>
                    </List.Item>
                </List>
                <List renderHeader={() => 'Send Feedback'}>
                    <List.Item onClick={() => {
                        Linking.openURL("mailto://developer@bugme.talonsoftwares.com")
                    }}
                               arrow="horizontal"
                               thumb="http://bugme.talonsoftwares.com/images/mail.png">
                        Click to send feedback to the developer
                    </List.Item>
                </List>

            </ScrollView>
        );
    }
}

const SettingsScreenWrapper = createForm()(SettingsScreen);
export default SettingsScreenWrapper;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});
