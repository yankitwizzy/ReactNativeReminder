import React from 'react';
import {
    StyleSheet,
    Text,
    View, ScrollView
} from 'react-native';
import {List, SearchBar, SwipeAction} from 'antd-mobile';
import {FontAwesome} from '@expo/vector-icons';

const Item = List.Item;

export default class HomeScreen extends React.Component {

    constructor (props) {
        super(props);
    }

    componentWillMount () {
        this.navigate = this.props.navigation.navigate;
    }

    static navigationOptions = ({navigation}) => ({
        title: 'Reminders',
        headerRight: <Text style={{marginRight: 13}}
                           onPress={() => {
                               navigation.navigate('ReminderDetails', {
                                   active: true,
                                   new: true
                               });
                           }}>
            <FontAwesome name="calendar-plus-o" size={24}/></Text>,
        headerLeft: null
    });

    loadReminders () {
        let i = 0;
        const reminders = this.props.screenProps.searching
            ? this.props.screenProps.filteredReminders : this.props.screenProps.reminders;
        if (reminders.length > 0) {
            return reminders.map((reminder) => {
                reminder.new = false;
                const thumb = reminder.active ? "http://bugme.talonsoftwares.com/images/active.png" : "http://bugme.talonsoftwares.com/images/inactive.png";
                return (
                    <SwipeAction key={`swipe-reminder-${i ++}`}
                                 style={{backgroundColor: 'gray'}}
                                 autoClose
                                 right={[
                                     {
                                         text: 'Delete',
                                         onPress: () => this.props.screenProps.deleteR(reminder),
                                         style: {
                                             backgroundColor: '#F4333C',
                                             color: 'white'
                                         },
                                     },
                                 ]}
                                 onOpen={() => console.log('global open')}
                                 onClose={() => console.log('global close')}
                    >
                        <Item key={`reminder-${i ++}`}
                              arrow="horizontal"
                              thumb={thumb}
                              onClick={() => this.navigate('ReminderDetails', reminder)}>
                            {reminder.name}
                        </Item>
                    </SwipeAction>
                )
            });
        } else {
            if (!this.props.screenProps.searching) {
                return (
                    <Item arrow="empty" multipleLine={true} wrap={true}
                          align="middle">
                        You have no reminders.
                        <Item.Brief>
                            Click icon on the top right to add.
                        </Item.Brief>
                    </Item>)
            } else {
                return (
                    <Item arrow="empty" multipleLine={true} wrap={true}
                          align="middle">
                        Your search didn't return any results.
                    </Item>)
            }

        }

    }

    render () {
        return (
            <View style={styles.container}>
                <SearchBar
                    cancelText="Cancel"
                    showCancelButton={false}
                    onCancel={(val)=>{ console.log(this.refs, val); }}
                    onChange={this._handleSearch.bind(this)}
                    placeholder='Type Here to search...'/>
                <ScrollView>
                    <List className="my-list">
                        {this.loadReminders()}
                    </List>
                </ScrollView>
            </View>
        );
    }

    _handleSearch (searchString) {
        this.props.screenProps.search(searchString);
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    scrollview: {}
});
