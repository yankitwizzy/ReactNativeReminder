import React from 'react';
import uuidv4 from 'uuid/v4';
import {ScrollView, StyleSheet, View, Text} from 'react-native';
import {
    Button,
    List,
    InputItem,
    DatePicker,
    Picker,
    Switch
} from 'antd-mobile';
import moment from 'moment';
import {createForm} from 'rc-form';
import enUs from 'antd-mobile/lib/date-picker/locale/en_US';
import FormConstants from "../constants/FormConstants";

class ReminderDetailsScreen extends React.Component {

    constructor (props) {
        super(props);
        this._save = this._save.bind(this);
        this._delete = this._delete.bind(this);
        this._update = this._update.bind(this);
        this._cancel = this._cancel.bind(this);
    }

    static navigationOptions = ({navigation}) => ({
        title: navigation.state.params.new ? "Add" : "Edit"
    });

    _save = () => {
        const reminder = this._getFormValue(true);
        if (reminder) {
            this.props.screenProps.add(reminder);
            this._cancel();
            setTimeout(() => {
                this.props.screenProps.scheduleNotificationsForNewReminderAction(reminder);
            }, 1000);

        }
    }

    _update = () => {
        const reminder = this._getFormValue(false);

        if (reminder) {
            const startTimeHours = Number(moment(reminder.startTime).format("H"));
            const startTimeMinutes = Number(moment(reminder.startTime).format("mm"));
            const endTimeHours = Number(moment(reminder.endTime).format("H"));
            const endTimeMinutes = Number(moment(reminder.endTime).format("mm"));

            const todayStartTime = moment().startOf("day").add({
                hours: startTimeHours,
                minutes: startTimeMinutes
            });
            const todayEndTime = moment().startOf("day").add({
                hours: endTimeHours,
                minutes: endTimeMinutes
            });
            reminder.startTime = todayStartTime;
            reminder.endTime = todayEndTime;

            this.props.screenProps.update(reminder);
            this._cancel();
            setTimeout(() => {
                this.props.screenProps.rescheduleNotificationsForUpdatedReminderAction(reminder);
            }, 1000);
        }
    }

    _cancel = () => {
        this.props.navigation.navigate('Home');
    }

    _validateFields = (values) => {
        let error = "";
        if (! values.name) {
            error += "Reminder name is required\n";
        }
        if (moment(values.startTime).isAfter(moment(values.endTime))
            || moment(values.endTime).diff(moment(values.startTime), "hours") < 1) {
            error += "End time should be at least an hour after start time";
        }
        return error;
    }

    _getFormValue = (isNew) => {
        const values = this.props.form.getFieldsValue();
        const error = this._validateFields(values);
        if (error) {
            window.alert(error);
            return null;
        }

        const reminder = {};
        if (isNew) {
            reminder.id = uuidv4();
        } else {
            reminder.id = this.props.navigation.state.params.id;
        }
        reminder.name = values.name;
        reminder.description = values.description || "";
        reminder.startTime = values.startTime;
        reminder.endTime = values.endTime;
        reminder.frequency = values.frequency[0];
        reminder.interval = values.interval[0];
        reminder.active = values.active;
        reminder.notificationIds = [];
        // reminder.repeat = values.repeat;
        return reminder;
    }

    _delete = () => {
        const reminder = this._getFormValue(false);
        if (reminder) {
            this.props.screenProps.deleteR(reminder);
            this._cancel();
        }
    }

    render () {

        const {getFieldProps} = this.props.form;

        const startTimeHours = Number(moment(this.props.screenProps.settings.startTime).format("H"));
        const startTimeMinutes = Number(moment(this.props.screenProps.settings.startTime).format("mm"));
        const endTimeHours = Number(moment(this.props.screenProps.settings.endTime).format("H"));
        const endTimeMinutes = Number(moment(this.props.screenProps.settings.endTime).format("mm"));
        const startTime = moment()
            .startOf("day")
            .add(startTimeHours, "hours")
            .add(startTimeMinutes, "minutes");
        const endTime = moment()
            .startOf("day")
            .add(endTimeHours, "hours")
            .add(endTimeMinutes, "minutes");

        return (
            <ScrollView style={styles.container}>
                <View>
                    <List renderHeader={() => 'Reminder Name & Description'}>
                        <InputItem {...getFieldProps('name', {
                            initialValue: this.props.navigation.state.params.name || null,
                            rules: [{required: true, type: "string"}]
                        })}
                                   clear
                                   placeholder="Name"
                        />
                        <InputItem {...getFieldProps('description',
                            {initialValue: this.props.navigation.state.params.description || null})}
                                   clear
                                   placeholder="Description"
                        />
                    </List>
                    <List renderHeader={() => 'Notification Settings'}>
                        <DatePicker locale={enUs}
                                    extra=""
                                    mode="time"
                                    {...getFieldProps('startTime',
                                        {initialValue: this.props.navigation.state.params.startTime ? moment(this.props.navigation.state.params.startTime) : moment(startTime)})}
                                    minuteStep={1}>
                            <List.Item thumb="http://bugme.talonsoftwares.com/images/start-icon2.png"
                                       arrow="horizontal">Start Time</List.Item>
                        </DatePicker>
                        <DatePicker locale={enUs} extra=""
                                    mode="time"
                                    {...getFieldProps('endTime',
                                        {initialValue: this.props.navigation.state.params.endTime ? moment(this.props.navigation.state.params.endTime) : moment(endTime)})}
                                    minuteStep={1}>
                            <List.Item thumb="http://bugme.talonsoftwares.com/images/stop.png" arrow="horizontal">End
                                Time</List.Item>
                        </DatePicker>
                        <Picker {...getFieldProps('frequency', {initialValue: [this.props.navigation.state.params.frequency || this.props.screenProps.settings.frequency]})}
                                title="Frequency"
                                okText="Ok"
                                dismissText="Cancel"
                                data={FormConstants.reminderFrequency}>
                            <List.Item thumb="http://bugme.talonsoftwares.com/images/sound.png" arrow="horizontal"
                                       onClick={this.onClick}>Frequency</List.Item>
                        </Picker>
                        <Picker {...getFieldProps('interval', {initialValue: [this.props.navigation.state.params.interval || this.props.screenProps.settings.interval]})}
                                title="Interval"
                                okText="Ok"
                                dismissText="Cancel"
                                data={FormConstants.reminderInterval}>
                            <List.Item thumb="http://bugme.talonsoftwares.com/images/interval.png" arrow="horizontal"
                                       onClick={this.onClick}>Interval</List.Item>
                        </Picker>
                    </List>
                    <List renderHeader={() => 'State'}>
                        <List.Item thumb="http://bugme.talonsoftwares.com/images/active-icon.png"
                                   extra={<Switch
                                       {...getFieldProps('active', {
                                           initialValue: this.props.navigation.state.params.active,
                                           valuePropName: 'checked'
                                       })}
                                   />}
                        >Active</List.Item>
                    </List>
                    {! this.props.navigation.state.params.new &&
                    <Button onClick={this._update} style={styles.button} type="primary" className="btn">Update</Button>}
                    {! this.props.navigation.state.params.new &&
                    <Button onClick={this._delete} style={styles.button} type="warning" className="btn">Delete</Button>}
                    {this.props.navigation.state.params.new &&
                    <Button onClick={this._save} style={styles.button}
                            type="primary" className="btn">Add</Button>}
                    {this.props.navigation.state.params.new &&
                    <Button onClick={this._cancel} style={styles.button}
                            type="warning" className="btn">Cancel</Button>}
                </View>
            </ScrollView>
        );
    }
}

const ReminderDetailsScreenWrapper = createForm()(ReminderDetailsScreen);
export default ReminderDetailsScreenWrapper;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    button: {
        margin: 5
    }
});
