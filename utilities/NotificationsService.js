import {Notifications} from "expo";
import FormConstants from "../constants/FormConstants";
import moment from "moment";

const appendToLastNotificationBody = "\n *** This is the last notification for this reminder. " +
    "If you want to reschedule it, then please tap to open it in the app *** ";

/**
 * notification constructor
 * @param title - string
 * @param body - string
 * @param data - string
 * @param sound - boolean
 * @constructor
 */
export function Notification (title, body, data, reminderId, sound = true) {
    this.title = title;
    this.body = body;
    this.data = data;
    this.sound = sound;
    this.reminderId = reminderId;
};

/**
 * scheduling options constructor
 * @param time - date or number
 * @param repeat - 'minute', 'hour', 'day', 'week', 'month', or 'year'
 * @constructor
 */
export function SchedulingOptions (time, repeat) {
    this.time = time;
    this.repeat = repeat;
}

/**
 * generates a random date between a start and an end date
 * @param start
 * @param end
 * @param startHour
 * @param endHour
 * @param interval
 * @returns {Date}
 */
export function randomDate (start, end, startHour, endHour, interval) {
    if (moment(start).isBefore(moment())) {
        start = Number(moment().format("x"));
    }

    let date = new Date((start + (Math.random() * (end - start))));
    let hour, minute;

    if (interval === FormConstants.intervals.hourly) {
        endHour = startHour + 1;
    }

    hour = Math.floor(Math.random() * (endHour - startHour + 1)) + startHour;
    date.setHours(hour);

    if (hour === startHour) {
        let startMinutes = Number(moment(start).format("mm"));
        minute = Math.floor(Math.random() * (60 - startMinutes + 1)) + startMinutes;
    } else if (hour === endHour) {
        let startMinutes = Number(moment(start).format("mm"));
        minute = Math.floor(Math.random() * (startMinutes - 0 + 1)) + 0;
    } else {
        minute = Math.floor(Math.random() * (60 - 1 + 1)) + 1;
    }
    date.setMinutes(minute);

    return date;
}

/**
 * get start and end time of a reminder
 * @param reminder
 * @returns {[startTime,endTime,nextStartTime,nextEndTime]}
 */
const getStartAndEndDates = (reminder) => {

    let now = moment();

    const startTimeHours = Number(moment(reminder.startTime).format("H"));
    const startTimeMinutes = Number(moment(reminder.startTime).format("mm"));
    const endTimeHours = Number(moment(reminder.endTime).format("H"));
    const endTimeMinutes = Number(moment(reminder.endTime).format("mm"));

    const todayStartTime = moment().startOf("day").add(startTimeHours, "hours").add(startTimeMinutes, "minutes");
    const todayEndTime = moment().startOf("day").add(endTimeHours, "hours").add(endTimeMinutes, "minutes");

    if (moment(now).isBefore(todayStartTime)) {
        now = todayStartTime;
    }

    if (moment(now).isAfter(moment(todayEndTime))) {
        now = moment(todayStartTime).add(1, "days");
    }

    const startDate = now;

    let endDate, nextStartDate, nextEndDate;

    switch (reminder.interval) {
        case FormConstants.intervals.hourly:
            endDate = moment(now).add(1, "hours");
            if (moment(endDate).isAfter(todayEndTime)) {
                endDate = todayEndTime;
            }
            nextStartDate = moment(endDate).add(1, "minutes");
            if (moment(nextStartDate).isAfter(moment(todayEndTime)) && moment(nextStartDate).isSame(moment(todayEndTime), "day")) {
                nextStartDate = todayStartTime.add(1, "days");
            }
            nextEndDate = moment(nextStartDate).add(1, "hours");
            if (moment(nextEndDate).isAfter(moment(todayEndTime))) {
                nextEndDate = todayEndTime;
            }
            break;
        case FormConstants.intervals.daily:
            endDate = moment(now).startOf("day").add({hours: endTimeHours, minutes: endTimeMinutes});
            nextStartDate = moment(endDate).add(2, "minutes");
            if (moment(nextStartDate).isAfter(moment(todayEndTime))) {
                nextStartDate = moment(todayStartTime).add(1, "days");
            }
            nextEndDate = moment(nextStartDate).startOf("day").add({hours: endTimeHours, minutes: endTimeMinutes});
            break;
        case FormConstants.intervals.weekly:
            endDate = moment(now).endOf("week").startOf("day").add({hours: endTimeHours, minutes: endTimeMinutes});
            nextStartDate = moment(endDate).endOf("day").add({
                seconds: 1,
                minutes: startTimeMinutes,
                hours: startTimeHours
            });
            nextEndDate = moment(nextStartDate).endOf("week");
            break;
        case FormConstants.intervals.monthly:
            endDate = moment(now).endOf("month"); // add(1, "months");
            nextStartDate = moment(endDate).add(1, "second");
            nextEndDate = moment(nextStartDate).endOf("month");
            break;
        case FormConstants.intervals.yearly:
            endDate = moment(now).endOf("year"); // add(1, "years");
            nextStartDate = moment(endDate).add(1, "seconds");
            nextEndDate = moment(nextStartDate).endOf("year");
            break;
    }

    return [
        moment(startDate).format("x"),
        moment(endDate).format("x"),
        moment(nextStartDate).format("x"),
        moment(nextEndDate).format("x")
    ];
};

/**
 * schedules a local notification
 * @param localNotification
 * @param schedulingOptions
 * @param dispatch
 * @param actionToDispatch
 * @param reminderId
 */
export function scheduleNotification (localNotification, schedulingOptions, dispatch, actionToDispatch, reminderId) {
    Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions)
        .then((notificationId) => {
            dispatch(actionToDispatch(reminderId, notificationId));
        }).catch((err) => {
        console.log("error occurred when setting notification", err);
    });
};

/**
 * cancels a local notification
 * @param notificationId
 */
export const cancelNotification = (notificationId) => {
    Notifications.cancelScheduledNotificationAsync(notificationId);
};

/**
 * cancels all scheduled local notifications
 */
export const cancelAllNotifications = () => {
    Notifications.cancelAllScheduledNotificationsAsync();
};

/**
 * reschedule a notification
 * @param notificationData
 * @param dispatch
 * @param actionToDispatch
 */
export const rescheduleNotificationsForReminder = (notificationData, dispatch, actionToDispatch) => {

    let clonedReminder = JSON.parse(JSON.stringify(notificationData.reminder));
    clonedReminder.notificationIds = [];
    const startTimeHours = Number(moment(clonedReminder.startTime).format("H"));
    const startTimeMinutes = Number(moment(clonedReminder.startTime).format("mm"));
    const endTimeHours = Number(moment(clonedReminder.endTime).format("H"));
    const endTimeMinutes = Number(moment(clonedReminder.endTime).format("mm"));
    clonedReminder.startTime = moment(Number(notificationData.nextStartDate))
        .startOf("day")
        .add(startTimeHours, "hours")
        .add(startTimeMinutes, "minutes");
    clonedReminder.endTime = moment(Number(notificationData.nextEndDate))
        .startOf("day")
        .add(endTimeHours, "hours")
        .add(endTimeMinutes, "minutes");

    const notification = new Notification(clonedReminder.name, clonedReminder.description || "#bugMeTillIDoIt", null);
    const dates = [];
    const frequency = parseInt(clonedReminder.frequency);
    const startAndEndDates = getStartAndEndDates(clonedReminder);
    for (i = 0; i < frequency; i ++) {
        let startHours, endHours;

        if (clonedReminder.interval === FormConstants.intervals.hourly) {
            startHours = parseInt(moment(Number(startAndEndDates[0])).format("H"));
            endHours = parseInt(moment(Number(startAndEndDates[1])).format("H"));
        } else {
            startHours = parseInt(moment(clonedReminder.startTime).format("H"));
            endHours = parseInt(moment(clonedReminder.endTime).format("H"));
        }

        const randomlyDated = randomDate(Number(startAndEndDates[0]),
            Number(startAndEndDates[1]), startHours, endHours, clonedReminder.interval);
        dates.push(randomlyDated.getTime());
    }
    dates.sort((a, b) => {
        return a - b;
    });

    let i = 1;
    dates.forEach((date) => {
        // const schedulingOption = new SchedulingOptions(date, reminder.repeat ? reminder);
        if (i === dates.length) {
            clonedReminder.startTime = clonedReminder.startTime.toString();
            clonedReminder.endTime = clonedReminder.endTime.toString();
            notification.body = notification.body + appendToLastNotificationBody;
            notification.data = {
                last: true,
                reminder: clonedReminder,
                nextStartDate: startAndEndDates[2],
                nextEndDate: startAndEndDates[3]
            };
        } else {
            notification.data = null;
        }

        scheduleNotification(notification, {time: date}, dispatch, actionToDispatch, notificationData.reminder.id);
        i ++;
    });
    return clonedReminder;
};

/**
 * delete notifications attached to a deleted reminder
 * @param reminder
 */
export const deleteNotificationsForDeletedReminder = (reminder) => {
    if (reminder.notificationIds) {
        reminder.notificationIds.forEach((notificationId) => {
            cancelNotification(notificationId);
        });
    }
};

/**
 * reschedule notifications for an updated reminder
 * @param reminder
 * @param dispatch
 * @param actionToDispatch
 */
export const rescheduleNotificationsForUpdatedReminder = (reminder, dispatch, actionToDispatch) => {
    deleteNotificationsForDeletedReminder(reminder);
    reminder.notificationIds = [];
    return scheduleNotificationsForNewReminder(reminder, dispatch, actionToDispatch);
};

/**
 * schedule notifications for new reminder
 * @param reminder
 * @param dispatch
 * @param actionToDispatch
 * @returns {*}
 */
export const scheduleNotificationsForNewReminder = (reminder, dispatch, actionToDispatch) => {

    if (! reminder.active) {
        return reminder;
    }

    const notification = new Notification(reminder.name, reminder.description || "#bugMeTillIDoIt", null);
    const dates = [];
    const frequency = parseInt(reminder.frequency);
    const startAndEndDates = getStartAndEndDates(reminder);
    for (i = 0; i < frequency; i ++) {

        let startHours, endHours;

        if (reminder.interval === FormConstants.intervals.hourly) {
            startHours = parseInt(moment(Number(startAndEndDates[0])).format("H"));
            endHours = parseInt(moment(Number(startAndEndDates[1])).format("H"));
        } else {
            startHours = parseInt(moment(reminder.startTime).format("H"));
            endHours = parseInt(moment(reminder.endTime).format("H"));
        }

        const randomlyDated = randomDate(Number(startAndEndDates[0]),
            Number(startAndEndDates[1]), startHours, endHours, reminder.interval);
        dates.push(randomlyDated.getTime());
    }
    dates.sort((a, b) => {
        return a - b;
    })

    let i = 1;
    let notificationIds = [];
    dates.forEach((date) => {
        if (i === dates.length) {
            reminder.startTime = reminder.startTime.toString();
            reminder.endTime = reminder.endTime.toString();
            notification.body = notification.body + appendToLastNotificationBody;
            notification.data = {
                last: true,
                reminder: reminder,
                nextStartDate: startAndEndDates[2],
                nextEndDate: startAndEndDates[3]
            };
        }

        scheduleNotification(notification, {time: date}, dispatch, actionToDispatch, reminder.id);
        i ++;
    });
    return reminder;
};
