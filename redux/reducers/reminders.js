import AppInitialState from "./AppInitialState";
import {AsyncStorage} from 'react-native';
import {
    scheduleNotificationsForNewReminder,
    rescheduleNotificationsForReminder,
    rescheduleNotificationsForUpdatedReminder,
    deleteNotificationsForDeletedReminder
} from "../../utilities/NotificationsService";
import moment from "moment";

/** ACTION TYPES **/

const ADD_REMINDER = "ADD_REMINDER";
const DELETE_REMINDER = "DELETE_REMINDER";
const UPDATE_REMINDER = "UPDATE_REMINDER";
const GET_REMINDER = "GET_REMINDER";
const SEARCH_REMINDER = "SEARCH_REMINDER";
const FETCH_DATA = "FETCH_DATA";
const DATA_FETCHED = "DATA_FETCHED";
const RESCHEDULE_REMINDER = "RESCHEDULE_REMINDER";
const ADD_NOTIFICATION_ID = "ADD_NOTIFICATION_ID";
const SETTING_UPDATED = "SETTING_UPDATED";

/** ACTION CREATORS **/

export const fetchReminders = () => {
    return {
        type: FETCH_DATA,
        payload: {}
    };
};

export const updateSetting = (val, key) => {
    return {
        type: SETTING_UPDATED,
        payload: { val: val, key: key }
    };
};

export const rescheduleReminder = (notificationData) => {
    return {
        type: RESCHEDULE_REMINDER,
        payload: { notificationData: notificationData }
    }
};

export const fetchedReminders = (state) => {
    return {
        type: DATA_FETCHED,
        payload: { appState: state }
    }
};

export const add = (reminder) => {
    return {
        type: ADD_REMINDER,
        payload: {
            reminder,
        }
    }
};

export const update = (reminder) => {
    return {
        type: UPDATE_REMINDER,
        payload: {
            reminder,
        }
    }
};

export const deleteR = (reminder) => {
    return {
        type: DELETE_REMINDER,
        payload: {
            reminder,
        }
    }
};

export const addNotificationIdToReminder = (reminderId, notificationId) => {
    return {
        type: ADD_NOTIFICATION_ID,
        payload: {
            reminderId,
            notificationId
        }
    }
};

export const scheduleNotificationsForNewReminderAction = (reminder) => {
    return (dispatch) => {
        if (reminder.active) {
            scheduleNotificationsForNewReminder(reminder, dispatch, addNotificationIdToReminder);
        }
    };
};

export const rescheduleNotificationsForUpdatedReminderAction = (reminder) => {
    return (dispatch) => {
        if (reminder.active) {
            rescheduleNotificationsForUpdatedReminder(reminder, dispatch, addNotificationIdToReminder);
        }
    };
};

export const rescheduleNotificationsForReminderAction = (notificationData) => {
    return (dispatch) => {
        if (notificationData.reminder.active) {
            rescheduleNotificationsForReminder(notificationData, dispatch, addNotificationIdToReminder);
        }
    };
};

export const search = (searchString) => {
    return {
        type: SEARCH_REMINDER,
        payload: {
            searchString,
        }
    }
};

async function updateDB (state) {
    try {
        await AsyncStorage.setItem("data", JSON.stringify(state));
    } catch (error) {
        // Error saving data
        console.log("error saving data");
    }
};

/** REDUCER **/

export default function reducer (state = AppInitialState, action) {

    switch (action.type) {
        case DATA_FETCHED:
            let newState = JSON.parse(JSON.stringify(action.payload.appState));
            if (newState.hasOwnProperty("settings") && typeof newState.settings.startTime === "undefined") {
                newState.settings = state.settings;
            }
            return {
                ...newState
            };
        case ADD_NOTIFICATION_ID:
            newState = JSON.parse(JSON.stringify(state));
            let indexOfUpdate = -1;
            let reminderToUpdate = newState.reminders.find((reminder, index) => {
                const reminderId = action.payload.reminderId;
                indexOfUpdate = index;
                return reminder.id === reminderId;
            });
            if (reminderToUpdate) {
                reminderToUpdate.notificationIds.push(action.payload.notificationId);
                newState.reminders[indexOfUpdate] = reminderToUpdate;
                updateDB(newState);
            } else {
                console.log("couldn't find reminder");
            }
            return {
                ...newState
            };
            break;
        case RESCHEDULE_REMINDER:
            newState = JSON.parse(JSON.stringify(state));
            const reminderFromData = action.payload.notificationData.reminder;
            indexOfUpdate = -1;
            reminderToUpdate = newState.reminders.find((reminder, index) => {
                const reminderId = reminderFromData.id;
                indexOfUpdate = index;
                return reminder.id === reminderId;
            });
            if (reminderToUpdate) {
                deleteNotificationsForDeletedReminder(reminderToUpdate)
                reminderToUpdate.notificationIds = [];
                newState.reminders[indexOfUpdate] = reminderToUpdate;
                updateDB(newState);
            }
            return { ...newState };
        case ADD_REMINDER:
            newState = JSON.parse(JSON.stringify(state));
            let addedReminder = JSON.parse(JSON.stringify(action.payload.reminder));
            newState.reminders.push(addedReminder);
            updateDB(newState);
            return {
                ...newState
            };
        case UPDATE_REMINDER:
            // when you update bring the day up to today for any reminder
            newState = JSON.parse(JSON.stringify(state));
            indexOfUpdate = -1;
            reminderToUpdate = newState.reminders.find((reminder, index) => {
               const reminderId = action.payload.reminder.id;
               indexOfUpdate = index;
               return reminder.id === reminderId;
            });
            if (reminderToUpdate) {
                let uReminder = action.payload.reminder;
                deleteNotificationsForDeletedReminder(uReminder);
                uReminder.notificationIds = [];
                //rescheduleNotificationsForUpdatedReminder(uReminder);
                newState.reminders[indexOfUpdate] = uReminder;
                updateDB(newState);
            }
            return { ...newState };
        case DELETE_REMINDER:
            newState = JSON.parse(JSON.stringify(state));
            indexOfUpdate = -1;
            let reminderToDelete = newState.reminders.find((reminder, index) => {
                const reminderId = action.payload.reminder.id;
                indexOfUpdate = index;
                return reminder.id === reminderId;
            });

            if (reminderToDelete && reminderToDelete.hasOwnProperty("id")) {
                deleteNotificationsForDeletedReminder(reminderToDelete);
                newState.reminders.splice(indexOfUpdate, 1);
                updateDB(newState);
            }
            return { ...newState };
        case SEARCH_REMINDER:
            newState = JSON.parse(JSON.stringify(state));
            if (action.payload.searchString) {
                newState.searching = true;
                let searchResults = newState.reminders.filter((reminder) => {
                    let searchString = action.payload.searchString.toLowerCase();
                    return ((reminder.name.toLowerCase().includes(searchString))
                        || (reminder.description
                            && reminder.description
                                .toLowerCase()
                                .includes(searchString)));
                });
                newState.filteredReminders = searchResults;
                return {...newState};
            }
            else {
                newState.searching = false;
                newState.filteredReminders = [];
                return {...newState};
            }
        case SETTING_UPDATED:
            newState = JSON.parse(JSON.stringify(state));

            if (action.payload.key === "startTime" || action.payload.key === "endTime") {
                const hours = Number(moment(action.payload.val).format("H"));
                const minutes = Number(moment(action.payload.val).format("mm"));
                const todayTime = moment().startOf("day").add(hours, "hours").add(minutes, "minutes");
                newState.settings[action.payload.key] = todayTime;
            } else {
                newState.settings[action.payload.key] = action.payload.val[0];
            }

            updateDB(newState);
            return { ...newState };
        default:
            return state
    }
}