import moment from "moment";
import FormConstants from "../../constants/FormConstants";

const now = moment();
const later = moment().add(1, "hours");

let AppInitialState = {
    reminders: [],
    filteredReminders: [],
    settings: {
        startTime: now,
        endTime: later,
        frequency: "5",
        interval: FormConstants.intervals.daily
    },
    searching: false
};
export default AppInitialState;