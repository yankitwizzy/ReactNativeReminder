import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import reminders from './reducers/reminders';
import AppInitialState from './reducers/AppInitialState';

const reducer = combineReducers({
    reminders,
});

const middleware = applyMiddleware(thunk);

export default function createAppStore(initialValue = AppInitialState) {
    let store;

    if (process.env.NODE_ENV === 'development') {
        // Development mode with Redux DevTools support enabled.
        const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Prevents Redux DevTools from re-dispatching all previous actions.
            shouldHotReload: false,
        }) : compose
        // Create the redux store.
        store = createStore(reminders, initialValue, composeEnhancers(middleware));
    } else {
        // Production mode.
        store = createStore(reminders, initialValue, middleware);
    }

    return store;
}