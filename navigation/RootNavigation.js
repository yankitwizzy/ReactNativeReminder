import React from 'react';
import { Provider } from 'react-redux';
import createAppStore from '../redux/index';
import RootNavigation from '../containers/RootNavigation';

const store = createAppStore();

const RootNavigationR = () => (
    <Provider store={store}>
        <RootNavigation/>
    </Provider>
);

export default RootNavigationR;
