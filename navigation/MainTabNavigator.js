/**
 * @flow
 */

import React from 'react';
import {FontAwesome} from '@expo/vector-icons';
import {TabNavigator, TabBarBottom} from 'react-navigation';

import Colors from '../constants/Colors';

import HomeScreen from '../screens/HomeScreen';
import SettingsScreen from '../screens/SettingsScreen';

const MainTabNavigator = TabNavigator(
    {
        Home: {
            screen: HomeScreen,
        },
        Settings: {
            screen: SettingsScreen,
        },
    },
    {
        navigationOptions: ({navigation}) => ({
            // Set the tab bar icon
            tabBarIcon: ({focused}) => {
                const {routeName} = navigation.state;
                let iconName;
                switch (routeName) {
                    case 'Home':
                        iconName = 'calendar-o';
                        break;
                    case 'Settings':
                        iconName = 'cog';
                }
                return (
                    <FontAwesome
                        name={iconName}
                        size={32}
                        color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
                    />
                );
            },
        }),
        // Put tab bar on bottom of screen on both platforms
        tabBarComponent: TabBarBottom,
        tabBarPosition: 'bottom',
        // Disable animation so that iOS/Android have same behaviors
        animationEnabled: true,
        // Don't show the labels
        tabBarOptions: {
            showLabel: true,
        },
    }
);

export default MainTabNavigator;