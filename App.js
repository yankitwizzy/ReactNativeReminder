import React from 'react';
import {Platform, StatusBar, StyleSheet, View} from 'react-native';
import {AppLoading, Permissions} from 'expo';
import {FontAwesome} from '@expo/vector-icons';
import RootNavigationR from './navigation/RootNavigation';

import cacheAssetsAsync from './utilities/cacheAssetsAsync';

export default class AppContainer extends React.Component {
    state = {
        appIsReady: false,
    };

    componentWillMount () {
        this._loadAssetsAsync();
    }

    componentDidMount () {
        this._getNotificationsPermissionAsync();
    }

    async _loadAssetsAsync () {
        try {
            await cacheAssetsAsync({
                fonts: [
                    FontAwesome.font,
                    {'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf')},
                    {'anticon': require('./assets/fonts/anticon.ttf')}
                ],
            });
        } catch (e) {
            console.warn(
                'There was an error caching assets (see: main.js), perhaps due to a ' +
                'network timeout, so we skipped caching. Reload the app to try again.'
            );
            console.log(e.message);
        } finally {
            this.setState({appIsReady: true});
        }
    }

    async _getNotificationsPermissionAsync () {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        if (status !== 'granted') {
            console.log("This app can't be used effectively if we can't send you notifications");
        }
    }

    render () {
        if (this.state.appIsReady) {
            return (
                <View style={styles.container}>
                    {Platform.OS === 'ios' && <StatusBar barStyle="default"/>}
                    {Platform.OS === 'android' &&
                    <View style={styles.statusBarUnderlay}/>}
                    <RootNavigationR/>
                </View>
            );
        } else {
            return <AppLoading/>;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    statusBarUnderlay: {
        height: 24,
        backgroundColor: 'rgba(0,0,0,0.2)',
    },
});
